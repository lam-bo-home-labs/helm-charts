Helm charts of the applications 

References
==========

- https://github.com/jkroepke/helm-secrets
- https://github.com/helmfile/vals/blob/main/README.md#gitlab-secrets
- https://www.cncf.io/blog/2024/01/16/installing-multiple-helm-charts-in-one-go-approach-2-using-helmfile/
- https://helmfile.readthedocs.io/en/latest/remote-secrets/#fetching-single-key

Prerequisites
=============

- helm
- helm-secrets plugin (i choose to use Gitlab CI secrets)
- helm-diff plugin
- helmfile

Installing helmfile
===================

https://www.cncf.io/blog/2024/01/16/installing-multiple-helm-charts-in-one-go-approach-2-using-helmfile/


Installing all charts
=====================

To access the Gitlab CI/CD secrets in secrets.yaml files, you need to export the GITLAB_TOKEN env variable with a personal access token

```
export GITLAB_TOKEN=<your access token>
export KUBECONFIG=~/.kube/home-labs-config
oci session authenticate
helmfile apply
```